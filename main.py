import src

RAW_DATA_PATH = "data/raw/labeled.csv"
PROCESSED_DATA_PATH = "data/interim/processed.csv"

if __name__ == "__main__":
    src.process_data(RAW_DATA_PATH, PROCESSED_DATA_PATH)
    src.train_model(PROCESSED_DATA_PATH)
