import pandas as pd
from nltk.tokenize import wordpunct_tokenize
from pymorphy2 import MorphAnalyzer
import click
from tqdm import tqdm


def lemmatize(words, morph):
    new_words = []
    for word in words:
        new_words.append(morph.normal_forms(word)[0])
    return new_words


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def process_data(input_filepath, output_filepath):

    print('===== Building Features =====')
    data = pd.read_csv(input_filepath)
    texts, labels = data.comment, data.toxic

    morph = MorphAnalyzer()

    preprocessed_texts = []
    for text in tqdm(texts):
        preprocessed_texts.append(lemmatize(wordpunct_tokenize(text.lower()), morph))

    final_texts = [" ".join(word for word in text) for text in preprocessed_texts]
    final_data = pd.DataFrame(data={'text': final_texts, 'label': labels})
    final_data.to_csv(output_filepath, index=False)

    print('===== DONE =====')

if __name__ == "__main__":
    process_data()
