from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow import keras
import numpy as np
import pandas as pd
import click


def tokenize(text, tokenizer, max_len):
    seqs = tokenizer.texts_to_sequences(text)
    seqs = tf.keras.preprocessing.sequence.pad_sequences(
        seqs, padding="pre", maxlen=max_len
    )
    return seqs


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def train_model(input_filepath, output_filepath):
    print('===== Training Model =====')
    data = pd.read_csv(input_filepath)
    final_texts, labels = data.text, data.label
    X_train, X_val, y_train, y_val = train_test_split(
        final_texts, labels, test_size=0.3, stratify=labels
    )

    vocab_size = 35000
    max_len = 35
    emb_size = 16

    tok = Tokenizer(oov_token="<unk>", num_words=vocab_size)
    tok.fit_on_texts(final_texts)
    tok.word_index["<pad>"] = 0
    tok.index_word[0] = "<pad>"

    X_train = tokenize(X_train, tok, max_len)
    X_val = tokenize(X_val, tok, max_len)

    model_cnn = keras.Sequential(
        [
            layers.Embedding(vocab_size + 1, emb_size),
            layers.Conv1D(16, 3, activation="relu"),
            layers.MaxPooling1D(3),
            layers.Conv1D(16, 3, activation="relu"),
            layers.GlobalAveragePooling1D(),
            layers.Dense(16, activation="relu"),
            layers.Dropout(0.1),
            layers.Dense(1, activation="sigmoid"),
        ]
    )

    model_cnn.compile(optimizer="nadam", loss="BinaryCrossentropy", metrics=["accuracy"])

    callbacks = [
        EarlyStopping(
            monitor="val_loss", mode="min", patience=1, verbose=1, restore_best_weights=True
        )
    ]
    history_cnn = model_cnn.fit(
        x=X_train,
        y=np.array(y_train),
        epochs=20,
        batch_size=64,
        validation_data=(X_val, y_val),
        callbacks=callbacks,
    )
    print('===== DONE =====')
    data.to_csv(output_filepath, index=False)

if __name__ == "__main__":
    train_model()